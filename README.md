# Project Name

[![unlicense](https://img.shields.io/badge/un-license-brightgreen.svg)](http://unlicense.org "The Unlicense") [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme "RichardLitt/standard-readme")

> Short description of the project

This is a slightly longer description of the project.

## Table of Contents

- [Security](#security)
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Contribute](#contribute)
- [License](#license)

## Security

### Optional sections

## Background

### Optional sections

## Install

### Optional sections

## Usage

### Optional sections

## API

### Optional sections

## Contribute

> Contributors to this project are expected to adhere to our [Code of Conduct](docs/CODE_OF_CONDUCT.md "Code of Conduct").

I welcome [issues](docs/issue_template.md "Issue template"), but I prefer [pull requests](docs/pull_request_template.md "Pull request template")! See the [contribution guidelines](contributing.md "Contributing") for more information.

### Optional sections

## License

This code is [set free](LICENSE).
